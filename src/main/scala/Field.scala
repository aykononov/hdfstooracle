class Field(index: Int, fieldHadoop: String, fieldOracle: String) {

  def index(): Int = {
    index
  }

  def fieldHadoop(): String = {
    fieldHadoop
  }

  def fieldOracle(): String = {
    fieldOracle
  }
}
