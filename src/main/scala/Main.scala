import java.io.{BufferedReader, File, FileInputStream, FileReader}
import java.sql.SQLSyntaxErrorException
import java.util.Properties

import org.apache.hadoop.fs.Path
import org.apache.spark.sql.{AnalysisException, DataFrame, Row, SaveMode, SparkSession}
import org.apache.spark.sql.functions.{col, count, max, when}
import org.apache.spark.sql.types.{DataType, StringType, StructField, StructType}

object Main {
  val spark = SparkSession.builder
    .master("local[*]")
    .appName("ts_project")
    .config("spark.driver.memory", "4g")
    .config("spark.driver.maxResultSize", "2g")
    .config("spark.sql.shuffle.partitions", 8)
    .config("spark.executor.memory", "4g")
    .config("spark.sql.autoBroadcastJoinThreshold", 1 * 1024 * 1024 * 1024)
    .getOrCreate()

  spark.sparkContext.setLogLevel("ERROR")


  var connectionProperties: Properties = _
  var changesDF: DataFrame = _


  def main(args: Array[String]): Unit = {

    val parser = new Parser("config1.json")
    connectionProperties = parser.getConnectionProperties()
    val hadoopPath = parser.getHadoopPath()
    val flows = parser.getFlows()
    val inkTable = parser.getInkTable()

    try{
      changesDF = spark.read
        .option("delimiter", ";")
        .option("header", "true")
        .csv(hadoopPath + inkTable)
    } catch {
      case e: AnalysisException => {
        val schema = StructType(
          StructField("tableName", StringType, false) ::
            StructField("columnName", StringType, false) ::
            StructField("value", StringType, false) :: Nil)
        changesDF = spark.createDataFrame(spark.sparkContext.emptyRDD[Row], schema)
      }
    }

    changesDF.show()


    var flow: Flow = null
    var sourceDF: DataFrame = null
    var resultDF: DataFrame = null
    var newRowsDF: DataFrame = null
    var inkField = ""

    var i = 0
    while (i < flows.size) {
      flow = flows(i)
      inkField = flow.inkField()
      sourceDF = ReadWrite.readParquet(hadoopPath + flow.sourcePath())
      sourceDF.createOrReplaceTempView(flow.tableNameOracle())
      resultDF = spark.sql(flow.sql())
      resultDF = mappingFields(flow.fields(), resultDF)
      newRowsDF = getNewRows(resultDF, flow.tableNameOracle(), inkField)

      if (!newRowsDF.isEmpty) {
        ReadWrite.writeDFToDB(newRowsDF, flow.tableNameOracle(), SaveMode.Append)


        changesDF = changesDF.withColumn("value",
          when(changesDF("tableName").equalTo(flow.tableNameOracle()) && changesDF("columnName").equalTo(inkField),
            sourceDF.agg(max(inkField)).collect().head.getAs[String](0)).otherwise(changesDF("value")))
      }

      i += 1
    }

    ReadWrite.rewriteCsv(changesDF, hadoopPath, inkTable, inkTable + "Temp")
  }

  def mappingFields(fields: Seq[Field], dataFrame: DataFrame): DataFrame = {
    var j = 0
    var field: Field = null
    var df: DataFrame = dataFrame
    while (j < fields.size) {
      field = fields(j)
      df = df.withColumnRenamed(field.fieldHadoop(), field.fieldOracle())

      j += 1
    }

    df
  }

  def getNewRows(resultDF: DataFrame, tableName: String, inkField: String): DataFrame = {
    val inkValue = getInkValue(tableName, inkField)
    var newRowsDF: DataFrame = null
    if(isStringColumnOfDate(resultDF, inkField)) {
      val tempDF = resultDF.withColumn(inkField, resultDF(inkField).cast("Date"))
      newRowsDF = tempDF.where(tempDF(inkField) > inkValue)
          .withColumn(inkField, tempDF(inkField).cast("String"))

    } else {
      newRowsDF = resultDF.where(resultDF(inkField) > inkValue)
    }

    newRowsDF
  }

  def getInkValue(tableName: String, inkField: String): String = {
    val df = changesDF.select("value")
      .where(changesDF("columnName") === inkField && changesDF("tableName") === tableName)

    var value = ""
    try {
      value = df.collect().head.getAs[String](0)
    } catch {
      case e: NoSuchElementException => {
        val newRow = Seq((tableName, inkField, "0"))

        import spark.implicits._

        changesDF = changesDF.union(newRow.toDF("tableName", "columnName", "value"))

        value = "0"
      }
    }

    value
  }

  def isStringColumnOfDate(dataFrame: DataFrame, columnName: String): Boolean = {
    if(dataFrame.schema(columnName).dataType == StringType) {
      try {
        val partDF = dataFrame.limit(100).withColumn(columnName, dataFrame(columnName).cast("Date"))

        partDF.createOrReplaceTempView("part")

        val numbers = spark.sql(
          s"""
             |SELECT COUNT(${columnName})
             |FROM part
             |""".stripMargin)

        val nulls = numbers.collect().head.get(0)
        nulls == 100
      } catch {
        case e: AnalysisException => false
      }
    } else {
      false
    }
  }

}
