import java.io.FileReader
import java.util.Properties

import com.google.gson.{JsonArray, JsonObject, JsonParser}
class Parser() {
  private var flows: Seq[Flow] = _
  private var connectionProperties: Properties = _
  private var hadoopPath: String = _
  private var inkTable: String = _

  def this(fileName: String) = {
    this()
    val reader = new FileReader(fileName)
    val jsonParser = new JsonParser
    val jsonObject = jsonParser.parse(reader).getAsJsonObject
    this.flows = getFlowsFromJson(jsonObject)
    this.connectionProperties = getConnectionPropertiesFromJson(jsonObject)
    this.hadoopPath = jsonObject.get("hadoopPath").getAsString
    this.inkTable = jsonObject.get("inkTable").getAsString
  }

  def getFlows(): Seq[Flow] = {
    flows
  }

  def getConnectionProperties(): Properties = {
    connectionProperties
  }

  def getHadoopPath(): String = {
    hadoopPath
  }

  def getInkTable(): String = {
    inkTable
  }

  private def getFlowsFromJson(jsonObject: JsonObject): Seq[Flow] = {
    val flowsJson = jsonObject.get("flows").getAsJsonArray

    var flows: Seq[Flow] = Seq.empty[Flow]
    var flow: Flow = null
    var fields: Seq[Field] = Seq.empty[Field]
    var inkField = ""
    var sourcePath = ""
    var tableNameOracle = ""
    var sql = ""
    var flowJson: JsonObject = null

    var i = 0
    while (i < flowsJson.size()) {
      flowJson = flowsJson.get(i).getAsJsonObject

      inkField = flowJson.get("inkField").getAsString
      sourcePath = flowJson.get("sourcePath").getAsString
      tableNameOracle = flowJson.get("tableNameOracle").getAsString
      sql = flowJson.get("sql").getAsString
      fields = getFieldsFromJson(flowJson)
      flow = new Flow(fields, inkField, sourcePath, tableNameOracle, sql)

      flows = flows :+ flow

      i += 1
    }

    flows
  }

  private def getFieldsFromJson(jsonObject: JsonObject) = {
    val fieldsJson = jsonObject.get("fields").getAsJsonArray

    var index = 0
    var fieldHadoop = ""
    var fieldOracle = ""
    var field: Field = null
    var fields: Seq[Field] = Seq.empty[Field]
    var fieldJson: JsonObject = null

    var i = 0
    while (i < fieldsJson.size()) {
      fieldJson = fieldsJson.get(i).getAsJsonObject
      index = fieldJson.get("index").getAsInt
      fieldHadoop = fieldJson.get("fieldHadoop").getAsString
      fieldOracle = fieldJson.get("fieldOracle").getAsString
      field = new Field(index, fieldHadoop, fieldOracle)

      fields = fields :+ field

      i += 1
    }

    fields
  }

  private def getConnectionPropertiesFromJson(jsonObject: JsonObject): Properties = {
    val connectionPropertiesJson = jsonObject.get("connectionProperties").getAsJsonObject

    val user = connectionPropertiesJson.get("user").getAsString
    val password = connectionPropertiesJson.get("password").getAsString
    val url = connectionPropertiesJson.get("url").getAsString

    val connectionProperties = new Properties()
    connectionProperties.setProperty("user", user)
    connectionProperties.setProperty("password", password)
    connectionProperties.put("url", url)

    this.connectionProperties = connectionProperties

    connectionProperties
  }
}
