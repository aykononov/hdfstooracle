import Main.{connectionProperties, spark}
import org.apache.commons.httpclient.URI
import org.apache.hadoop.fs.Path
import org.apache.spark.sql.{DataFrame, SaveMode}
import org.json4s.DefaultFormats
import org.json4s.jackson.JsonMethods.parse

object ReadWrite {
  def firstReadFromHDFS(path: String): DataFrame = {
    val df = spark
      .read
      .parquet(path)

    writeDFToDB(df, "operation", SaveMode.Overwrite)

    df
  }

  def readParquet(path: String): DataFrame = {
    val df = spark.read
      .option("encoding", "Windows-1251")
      .parquet(path)

    df
  }

  def writeDFToDB(df: DataFrame, table: String, mode: SaveMode): Unit = {
    df.write
      .option("driver", "oracle.jdbc.OracleDriver")
      .mode(mode)
      .jdbc(connectionProperties.get("url").toString, table, connectionProperties)
  }

  def readDFFromDB(table: String): DataFrame = {
    val df = spark.read
      .option("driver", "oracle.jdbc.OracleDriver")
      .jdbc(connectionProperties.get("url").toString, table, connectionProperties)

    df
  }

  def readListFieldsFromJSON(json: String): List[Field] = {
    implicit val formats = DefaultFormats
    val fields = parse(json).extract[List[Field]]

    fields
  }

  def rewriteCsv(dataFrame: DataFrame, hdfsPath: String, fileName: String, tempFileName: String) = {
    dataFrame.write
      .option("delimiter", ";")
      .option("header", "true")
      .format("csv")
      .mode(SaveMode.Overwrite)
      .csv(hdfsPath + tempFileName)

    //val fs = org.apache.hadoop.fs.FileSystem.get(spark.sparkContext.hadoopConfiguration)
    val fs = org.apache.hadoop.fs.FileSystem.get(new java.net.URI(hdfsPath), spark.sparkContext.hadoopConfiguration)

    fs.setWorkingDirectory(new Path(hdfsPath))

    fs.delete(new Path(fileName), true)

    fs.rename(new Path(tempFileName), new Path(fileName))
  }
}
