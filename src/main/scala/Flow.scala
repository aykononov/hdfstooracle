class Flow(fields: Seq[Field], inkField: String, sourcePath: String, tableNameOracle: String, sql: String){

  def fields(): Seq[Field] = {
    fields
  }

  def inkField(): String = {
    inkField
  }

  def sourcePath(): String = {
    sourcePath
  }

  def tableNameOracle(): String = {
    tableNameOracle
  }

  def sql(): String = {
    sql
  }
}
