name := "HDFStoOracle"

version := "0.1"

scalaVersion := "2.11.12"

libraryDependencies += "org.apache.spark" %% "spark-sql" % "2.4.7"

libraryDependencies += "com.oracle.ojdbc" % "ojdbc8" % "19.3.0.0"
